'use strict';

const roomsData = require('./rooms.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const rooms = roomsData.map(r => ({
      RoomBaseID: r.RoomBaseID,
      RoomDescription: r.Description,
      SaltoExtID: r.CustomString1,
      SaltoID: r.RoomSpace.Booking.Entry.ID5,
      BookingStatus: r.RoomSpace.Booking.EntryStatusEnum,
      CheckinDate: r.RoomSpace.Booking.CheckInDate ? new Date(r.RoomSpace.Booking.CheckInDate) : null,
      CheckoutDate: r.RoomSpace.Booking.CheckOutDate ? new Date(r.RoomSpace.Booking.CheckOutDate) : null,
    }));
    return queryInterface.bulkInsert('Rooms', rooms, {}, {
      'RoomBaseID': {
        autoIncrement: true
      }
    });
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Rooms', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
