'use strict';
module.exports = (sequelize, DataTypes) => {
  const Room = sequelize.define('Room', {
    RoomBaseID: { type: DataTypes.INTEGER, primaryKey: true},
    RoomDescription: DataTypes.STRING,
    SaltoExtID: DataTypes.STRING,
    SaltoID: DataTypes.STRING,
    BookingStatus: DataTypes.STRING,
    CheckinDate: DataTypes.DATE,
    CheckoutDate: DataTypes.DATE
  }, {
    timestamps: false
  });
  Room.associate = function(models) {
    // associations can be defined here
  };
  return Room;
};