  require('dotenv').config();

  const path = require('path');
  const webpack = require('webpack');
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  const { CleanWebpackPlugin } = require('clean-webpack-plugin');

  module.exports = {
    mode: 'development',
    entry: {
      app: './public/app.js',
    },
    devtool: 'inline-source-map',
    devServer: {
      contentBase: './public',
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      },
      extensions: ['*', '.js', '.vue', '.json']
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader'
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.scss$/,
          loader: 'sass-loader',
            options: {
              sourceMap: true,
              // options...
            }
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        title: 'Balliol Oxford',
        template: 'public/index.html',
        inject: true
      }),
      new webpack.DefinePlugin({
          'process.env.SHOW_ROOM': JSON.stringify(process.env.SHOW_ROOM || 0)
      })
    ],
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, './dist'),
      publicPath: '/',
    },
    watch: true
  };
