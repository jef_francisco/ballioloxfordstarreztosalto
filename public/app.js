// document ready
// get data from node server (i.e http://localhost:8000/api/getFunctionBooking)
// render data

// import './app.scss'
import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import axios from 'axios';
import moment from 'moment';
import $ from 'jquery';
import _ from 'lodash';
import 'bulma/css/bulma.css'


const formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

const app = new Vue({
  el: '#app',
  data () {
    return {
      bookings: [],
      rooms: [],
      showRoom: parseInt(process.env.SHOW_ROOM, 10),
      startDate: new Date()
    }
  },
  components: {
    Datepicker
  },
  mounted () {
    // console.log('SHOW_ROOM', process.env.SHOW_ROOM)
    const dateParam = new Date();
    const dateLeft = moment(dateParam).subtract(1, 'day');
    const dateRight = moment(dateParam).add(1, 'day');

    axios
        .get(`/getRoomBase?`)
        .then( ({data}) => {
            const RoomBaseIDs = _.map(data,'RoomBaseID');
            
            return axios.get(`/getRoomSpace?RoomBaseIDs=${RoomBaseIDs}`)
                .then(roomSpaceResponse => {
                
                    const RoomSpaceIDs = _.map(roomSpaceResponse.data,'RoomSpaceID');
                    data = _.map(data, d => {
                        d.RoomSpace = _.find(roomSpaceResponse.data, {'RoomBaseID': d.RoomBaseID});
                        return d;
                    });
                    return {data, RoomSpaceIDs};
                
                }).then( ({data, RoomSpaceIDs}) => {
                    // const RoomStatus = 'InRoom';
                    return axios.get(`getBooking?RoomSpaceIDs=${RoomSpaceIDs}`)
                        .then(bookingResponse => {
                            // console.log(bookingResponse, 'bookings in roomspace');
                            
                            const EntryIDs = _.map(bookingResponse.data, 'EntryID')
                            data = _.map(data, d => {
                                d.RoomSpace.Booking = _.find(bookingResponse.data, { 'RoomSpaceID': d.RoomSpace.RoomSpaceID, 'EntryStatusEnum' : 'InRoom' }) || {};
                                return d;
                            });
                            return {data, EntryIDs}; 
                        
                        }).then( ({data, EntryIDs}) => {
                            return axios.get(`getEntry?EntryIDs=${EntryIDs}`)
                                .then(entryResponse => {
                                    data = _.map(data, d => {
                                        d.RoomSpace.Booking.Entry = _.find(entryResponse.data, { 'BookingID': d.RoomSpace.Booking.BookingID}) || {};
                                        return d;
                                    });
                                    // console.log('data', data);
                                    app.rooms = data;
                                    // return data;
                                })

                        })
                    })
        })

    axios
      .get(`/getFunctionBooking?_orderBy=DateStart.ASC&startDate=${formatDate(dateLeft)}&endDate=${formatDate(dateRight)}`)
      .then(response => {
        app.bookings = response.data;
      })
  },
  methods: {
    searchBooking : () => {
    
      var dateLeft = new Date();
      var dateRight = new Date();

      
        dateRight= moment(app.startDate).add(1, 'day');
        dateLeft= moment(app.startDate).subtract(1, 'day');
      
      axios
        .get(`/getFunctionBooking?_orderBy=DateStart.DESC&startDate=${formatDate(dateLeft)}&endDate=${formatDate(dateRight)}`)
        .then(response => {
           
          app.bookings = response.data;
        })
    },
    formatTime: (date) => {
        return moment(date).format('HH:mm')
    },
    formatDate: (date) =>{
        return moment(date).format('MMMM Do YYYY ')
    }
  }
})


