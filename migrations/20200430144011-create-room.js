'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Rooms', {
      RoomBaseID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      RoomDescription: {
        type: Sequelize.STRING
      },
      SaltoExtID: {
        type: Sequelize.STRING
      },
      SaltoID: {
        type: Sequelize.STRING
      },
      BookingStatus: {
        type: Sequelize.STRING
      },
      CheckinDate: {
        type: Sequelize.DATE
      },
      CheckoutDate: {
        type: Sequelize.DATE
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Rooms');
  }
};