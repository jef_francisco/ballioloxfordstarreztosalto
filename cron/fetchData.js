const fs = require('fs')
const path = require('path');

const api = require('../api')
const _ = require('lodash')


const fetchData = async () => {
    // fetch RoomBase
    let roomBases = await api.getRoomBase()
    const RoomBaseIDs = _.map(roomBases.data, 'RoomBaseID')

    // fetch RoomSpaces
    let roomSpaces = await api.getRoomSpace(RoomBaseIDs)
    const RoomSpaceIDs = _.map(roomSpaces.data,'RoomSpaceID')

    // fetch Bookings
    let bookings = await api.getBooking(RoomSpaceIDs)
    const EntryIDs = _.map(bookings.data,'EntryID')

    // fetch Entries
    let entries = await api.getEntry(EntryIDs)

    let data = transformData(roomBases.data, roomSpaces.data, bookings.data, entries.data)
    writeData(data);
    console.log(`${data.length} JSON data exported!`)
}

const transformData = (roomBases, roomSpaces, bookings, entries) => {
    return roomBases.map(room => {
        room.RoomSpace = _.find(roomSpaces, {'RoomBaseID': room.RoomBaseID}) || {};
        room.RoomSpace.Booking = _.find(bookings, {'RoomSpaceID': room.RoomSpace.RoomSpaceID, 'EntryStatusEnum' : 'InRoom' }) || {};
        room.RoomSpace.Booking.Entry = _.find(entries, {'BookingID': room.RoomSpace.Booking.BookingID}) || {};
        return room;
    });
}

const writeData = (data) => {
    const json = JSON.stringify(data, null, 2)
    fs.writeFileSync(path.join(__dirname, '../seeders/rooms.json'), json)
}

fetchData().catch(err => console.error(err));