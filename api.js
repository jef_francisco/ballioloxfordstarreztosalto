"use strict";

require('dotenv').config();

const axios = require('axios');
const _ = require('lodash');

const API_KEY = process.env.API_KEY;
const URL = process.env.BASE_URL;
const reqAction = '/select';
const FunctionBookingURL = '/FunctionBooking';
const RoomBaseURL = '/RoomBase';
const RoomSpaceURL = '/RoomSpace';
const BookingURL = '/Booking';
const EntryURL = '/Entry';
const AuthStr = 'Bearer '.concat(API_KEY); 

// OPERATOR MAP
const operatorMap = {
	'>': 'gt',
	'<': 'lt',
	'<=': 'lte',
	'>=': 'gte',
	'in' : 'in' 
}

// DATE FORMAT
const formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

// RELATED TABLES
const serializeRelatedTables = (relatedTables) => {
	return relatedTables.map(t => (`_relatedTables=${t}`));
}

// SERIALIZE PARAMETER
const serializeParams = (name, conditions, operatorMap) => {
	let params = conditions.map(t => {
		const op = t.operator;
		const rightOperand = t.rightOperand;
		const operator = encodeURIComponent(`_operator=${operatorMap[op]}`)
		return `${name}[${operator}]=${rightOperand}`
	})
	return params;
}

const paramsSerializer = (params) => {
	let strParams = [];
	for(var i in params) {
		if (i === '_relatedTables') {
			strParams = strParams.concat(serializeRelatedTables(params[i]));
		}
		else {
			strParams = strParams.concat(serializeParams(i, params[i], operatorMap));
		}
	}
	// console.log(strParams);
	return strParams.join('&');

}


const defaultDate = Date.now();

// PARAM FUNCTION BOOKING 
const paramFunctionBooking = {
	DateStart: [
		{
			operator: '>',
			rightOperand: defaultDate // -1
		},
		{
			operator: '<',
			rightOperand: defaultDate // +1
		},
	],
	'_relatedTables': ['FunctionBookingCatering','FunctionBookingCateringItem','FunctionRoomBooking']
}

// PARAM RoomBase
const paramRoomBase = {
	RoomBaseID: [
		{
			operator: '>',
			rightOperand: 0
		}
	],
	CustomString1: [
		{
			operator: '>',
			rightOperand: 0
		}
	]
}

// PARAM RoomSpace
const paramRoomSpace = {
	RoomBaseID: [
		{
			operator: 'in',
			rightOperand: []
		}
	],
}

const paramBooking = {
	RoomSpaceID: [
		{
			operator: 'in',
			rightOperand: []
		}
	],
}

// PARAM Entry
const paramEntry = {
	EntryID: [
		{
			operator: 'in',
			rightOperand: []
		}
	],

}



module.exports = {
	getFunctionBooking: (startDate, endDate) => {
		paramFunctionBooking.DateStart[0].rightOperand = formatDate(startDate);
		paramFunctionBooking.DateStart[1].rightOperand = formatDate(endDate);
		// console.log(params,'params');
		return axios.request({
			url: URL + reqAction + FunctionBookingURL, 
			method: 'get',	
			params: paramFunctionBooking,  
			paramsSerializer,
			headers: { 
				'cache-control': 'no-cache', 
				'Authorization': AuthStr,
			}}).catch(t => {
				
				if (t.response && t.response.status == 404 && t.response.data) {
					return {data: []};
				}
				throw t;
			})
	},

	getRoomBase: () => {
		return axios.request({
			url: URL + reqAction + RoomBaseURL,
			method: 'get',
			params: paramRoomBase,
			paramsSerializer,
			headers: {
				'cache-control' : 'no-cache',
				'Authorization' : AuthStr,
			}}).catch(t => {
				if (t.response && t.response.status == 404 && t.response.data) {
					return {data: []};
				}
				throw t;
			})
	},

	getRoomSpace: (RoomBaseIDs) => {
		paramRoomSpace.RoomBaseID[0].rightOperand = RoomBaseIDs;
		return axios.request({
			url: URL + reqAction + RoomSpaceURL,
			method: 'get',
			params: paramRoomSpace,
			paramsSerializer,
			headers: {
				'cache-control' : 'no-cache',
				'Authorization' : AuthStr,
			}}).catch(t => {
				if (t.response && t.response.status == 404 && t.response.data) {
					return {data: []};
				}
				throw t;
			})
	},
	getBooking: (RoomSpaceIDs) => {
		paramBooking.RoomSpaceID[0].rightOperand = RoomSpaceIDs;
		return axios.request({
			url: URL + reqAction + BookingURL,
			method: 'get',
			params: paramBooking,
			paramsSerializer,
			headers: {
				'cache-control' : 'no-cache',
				'Authorization' : AuthStr,
			}}).catch(t => {
				if (t.response && t.response.status == 404 && t.response.data) {
					return {data: []};
				}
				throw t;
			})
	},
	getEntry: (EntryIDs) => {
		paramEntry.EntryID[0].rightOperand = EntryIDs;
		return axios.request({
			url: URL + reqAction + EntryURL,
			method: 'get',
			params: paramEntry,
			paramsSerializer,
			headers: {
				'cache-control' : 'no-cache',
				'Authorization' : AuthStr,
			}}).catch(t => {
				if (t.response && t.response.status == 404 && t.response.data) {
					return {data: []};
				}
				throw t;
			})	
	}
	
}