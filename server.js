
require('dotenv').config(); // read .env files

const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');

const api = require('./api')

const app = express();
const port = process.env.PORT || 3000;

const config = require('./webpack.config.js');
const compiler = webpack(config);



app.use((req, res, next) => {

  // -----------------------------------------------------------------------
  // authentication middleware

  const auth = {login: process.env.HTTP_USERNAME , password: process.env.HTTP_PASSWORD } // change this

  // parse login and password from headers
  const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
  const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')

  // Verify login and password are set and correct
  if (login && password && login === auth.login && password === auth.password) {
    // Access granted...
    return next()
  }

  // Access denied...
  res.set('WWW-Authenticate', 'Basic realm="401"') // change this
  res.status(401).send('Authentication required.') // custom message

  // -----------------------------------------------------------------------

})

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath,
}));

app.use('/getFunctionBooking', function (req, res) {
	const startDate = req.query.startDate;
	const endDate = req.query.endDate;

	api.getFunctionBooking(startDate, endDate).then(response => {
		// If request is good...
		res.json(response.data);

	}).catch((error) => {
		res.json({
			'error ': error + ''
		});
	});

});

app.use('/getRoomBase', function(req, res){
  api.getRoomBase().then(response => {
    // If request is good...

    res.json(response.data);

  }).catch((error) => {
    res.json({
      'error ': error + ''
    });
  });

});

app.use('/getRoomSpace', function(req, res){
  const RoomBaseIDs = req.query.RoomBaseIDs.split(',');

  api.getRoomSpace(RoomBaseIDs).then(response => {
    // If request is good...

    res.json(response.data);

  }).catch((error) => {
    res.json({
      'error ': error + ''
    });
  });

});

app.use('/getBooking', function(req, res){
  const RoomSpaceIDs = req.query.RoomSpaceIDs.split(',');
  api.getBooking(RoomSpaceIDs).then(response => {
    // If request is good...

    res.json(response.data);

  }).catch((error) => {
    res.json({
      'error ': error + ''
    });
  });

});

app.use('/getEntry', function(req, res){
  const EntryIDs = req.query.EntryIDs.split(',');
  api.getEntry(EntryIDs).then(response => {
    // If request is good...

    res.json(response.data);

  }).catch((error) => {
    res.json({
      'error ': error + ''
    });
  });

});




// Listen for HTTP requests on port 3000
app.listen(port, () => {
  console.log('listening on %d', port);
});

